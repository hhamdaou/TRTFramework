#include <TRTFramework/Helpers.h>
#include <TRTFramework/Algorithm.h>
#include <xAODCore/AuxContainerBase.h>
#include <cmath>

xTRT::StrawRegion xTRT::getStrawRegion(const float eta) {
  float absEta = std::fabs(eta);
  if      ( absEta < 0.625 )  return xTRT::StrawRegion::BRL;
  else if ( absEta < 1.070 )  return xTRT::StrawRegion::BRLECA;
  else if ( absEta < 1.304 )  return xTRT::StrawRegion::ECA;
  else if ( absEta < 1.752 )  return xTRT::StrawRegion::ECAECB;
  else if ( absEta < 2.000 )  return xTRT::StrawRegion::ECB;
  else                        return xTRT::StrawRegion::NOTTRT;
}

xTRT::StrawRegion xTRT::getStrawRegion(const xAOD::TrackParticle* track) {
  return getStrawRegion(track->eta());
}

xTRT::SideRegion xTRT::getSideRegion(const int bec) {
  switch ( bec ) {
  case -2:
    return xTRT::SideRegion::SIDE_C;
  case -1:
  case  1:
    return xTRT::SideRegion::BARREL;
  case  2:
    return xTRT::SideRegion::SIDE_A;
  default:
    std::string msg = "bad value for bec: " + std::to_string(bec);
    XTRT_FATAL(msg);
    break;
  }
  return xTRT::SideRegion::NONTRT;
}

xTRT::SideRegion xTRT::getSideRegion(const xTRT::DriftCircle* dc) {
  int bec = xTRT::Algorithm::get(xTRT::Acc::bec,dc);
  return xTRT::getSideRegion(bec);
}

int xTRT::absoluteBarrelSL(const int sl, const int layer) {
  int abs_SL = sl;
  if ( layer > 0 )
    abs_SL += 19;
  if ( layer > 1 )
    abs_SL += 24;
  if ( layer > 2 || abs_SL > 72 )
    XTRT_FATAL("absoluteBarrelSL: Layer information is bad!");
  return abs_SL;
}

int xTRT::absoluteBarrelSL(const xTRT::DriftCircle* driftCircle) {
  int sl    = xTRT::Algorithm::get(xTRT::Acc::strawlayer,driftCircle);
  int layer = xTRT::Algorithm::get(xTRT::Acc::layer,driftCircle);
  return xTRT::absoluteBarrelSL(sl,layer);
}

int xTRT::absoluteEndCapSL(const int sl, const int wheel) {
  int abs_SL = sl;
  if ( wheel < 6 )
    abs_SL += 16 * wheel;
  else
    abs_SL += 96 + (wheel - 6) * 8;
  if ( abs_SL > 159 )
    XTRT_FATAL("absoluteEndCapSL: Layer information is bad!");
  return abs_SL;
}

int xTRT::absoluteEndCapSL(const xTRT::DriftCircle* driftCircle) {
  int sl  = xTRT::Algorithm::get(xTRT::Acc::strawlayer,driftCircle);
  int whl = xTRT::Algorithm::get(xTRT::Acc::layer,driftCircle);
  return xTRT::absoluteEndCapSL(sl,whl);
}

bool xTRT::truthMatched(const xAOD::Electron* electron) {
  static const SG::AuxElement::ConstAccessor<int> bkgTruthOrigin {"bkgTruthOrigin"};
  int true_type = 0, true_origin = 0, true_originbkg = 0;
  true_type   = xAOD::TruthHelpers::getParticleTruthType(*electron);
  true_origin = xAOD::TruthHelpers::getParticleTruthOrigin(*electron);
  if ( bkgTruthOrigin.isAvailable(*electron) ) true_originbkg = bkgTruthOrigin(*electron);

  bool istruthmatched = ( true_type == MCTruthPartClassifier::ParticleType::IsoElectron ||
                          ( true_type == MCTruthPartClassifier::ParticleType::BkgElectron &&
                            true_origin == MCTruthPartClassifier::ParticleOrigin::PhotonConv &&
                            ( true_originbkg == MCTruthPartClassifier::ParticleOrigin::TauLep ||
                              true_originbkg == MCTruthPartClassifier::ParticleOrigin::ZBoson ||
                              true_originbkg == MCTruthPartClassifier::ParticleOrigin::WBoson ) ) );
  return istruthmatched;
}

bool xTRT::truthMatched(const xAOD::Muon* muon) {
  int true_type = xAOD::TruthHelpers::getParticleTruthType(*muon);
  return (true_type == MCTruthPartClassifier::ParticleType::IsoMuon);
}

float xTRT::trackPHF(const xAOD::TrackParticle* track) {
  if ( xTRT::Acc::msosLink.isAvailable(*track) ) {
    const xTRT::MSOS* msos = nullptr;
    int nprec = 0;
    int ntrt  = 0;
    for ( const auto& trackState : xTRT::Acc::msosLink(*track) ) {
      msos = *trackState;
      if ( msos->detType() != 3 ) continue;
      ntrt++;
      if ( hitIsPrecision(msos) ) nprec++;
    }
    return ((float)nprec / ntrt);
  }
  return -1;
}

bool xTRT::failedExtension(const xAOD::TrackParticle* track) {
  if ( nTRT_PrecTube(track) == 0 && nTRT_Outlier(track) > 0 ) {
    return true;
  }
  return false;
}

xTRT::HitSummary xTRT::getHitSummary(const xAOD::TrackParticle* track,
                                     const xTRT::MSOS* msos,
                                     const xTRT::DriftCircle* driftCircle,
                                     const bool isMC) {
  xTRT::HitSummary hit;
  hit.HTMB        = (xTRT::Algorithm::get(xTRT::Acc::bitPattern, driftCircle,"bitPattern") & 131072) ? 1 : 0;
  hit.tot         =  xTRT::Algorithm::get(xTRT::Acc::tot,        driftCircle,"tot");
  hit.gasType     =  xTRT::Algorithm::get(xTRT::Acc::gasType,    driftCircle,"gasType");
  hit.bec         =  xTRT::Algorithm::get(xTRT::Acc::bec,        driftCircle,"bec");
  hit.layer       =  xTRT::Algorithm::get(xTRT::Acc::layer,      driftCircle,"layer");
  hit.strawlayer  =  xTRT::Algorithm::get(xTRT::Acc::strawlayer, driftCircle,"strawlayer");
  hit.strawnumber =  xTRT::Algorithm::get(xTRT::Acc::strawnumber,driftCircle,"strawnumber");
  hit.drifttime   =  xTRT::Algorithm::get(xTRT::Acc::drifttime,  driftCircle,"drifttime");
  hit.T0          =  xTRT::Algorithm::get(xTRT::Acc::T0,         driftCircle,"T0");
  hit.bitPattern  =  xTRT::Algorithm::get(xTRT::Acc::bitPattern, driftCircle,"bitPattern");

  hit.type       = xTRT::Algorithm::get(xTRT::Acc::type,      msos,"type");
  hit.localX     = xTRT::Algorithm::get(xTRT::Acc::localX,    msos,"localX");
  hit.localY     = xTRT::Algorithm::get(xTRT::Acc::localY,    msos,"localY");
  hit.localTheta = xTRT::Algorithm::get(xTRT::Acc::localTheta,msos,"localTheta");
  hit.localPhi   = xTRT::Algorithm::get(xTRT::Acc::localPhi,  msos,"localPhi");
  hit.HitZ       = xTRT::Algorithm::get(xTRT::Acc::HitZ,      msos,"HitZ");
  hit.HitR       = xTRT::Algorithm::get(xTRT::Acc::HitR,      msos,"HitR");
  hit.rTrkWire   = xTRT::Algorithm::get(xTRT::Acc::rTrkWire,  msos,"rTrkWire");

  hit.L             = xTRT::hitL(track,msos,driftCircle);
  hit.ZR            = xTRT::hitZR(track,driftCircle);
  hit.isPrec        = xTRT::hitIsPrecision(msos);
  hit.radius        = xTRT::hitRadius(msos,driftCircle);
  hit.corrDriftTime = xTRT::hitCorrectedDriftTime(driftCircle,isMC);

  return hit;
}

float xTRT::hitCorrectedDriftTime(const xTRT::DriftCircle* driftCircle, const bool isMC) {
  float dt     = xTRT::Algorithm::get(xTRT::Acc::drifttime,             driftCircle);
  float T0     = xTRT::Algorithm::get(xTRT::Acc::T0,                    driftCircle);
  float dtToTC = xTRT::Algorithm::get(xTRT::Acc::driftTimeToTCorrection,driftCircle);
  float dtHTC  = xTRT::Algorithm::get(xTRT::Acc::driftTimeHTCorrection, driftCircle);
  int   gType  = xTRT::Algorithm::get(xTRT::Acc::gasType,               driftCircle);

  float cdt = dt - T0;
  if ( isMC ) cdt += 8;
  if ( gType == 1 || gType == 6 ) gType += (dtHTC - dtToTC);
  return cdt;
}

float xTRT::hitError(const xTRT::MSOS* msos) {
  float ubr = xTRT::Algorithm::get(xTRT::Acc::unbiasedResidualX,msos);
  float ubp = xTRT::Algorithm::get(xTRT::Acc::unbiasedPullX,    msos);
  float br  = xTRT::Algorithm::get(xTRT::Acc::biasedResidualX,  msos);
  float bp  = xTRT::Algorithm::get(xTRT::Acc::biasedPullX,      msos);
  float r1  = (ubr/ubp);
  float r2  = (br/bp);
  float de  = std::sqrt(0.5*(r1*r1 + r2*r2));
  return de;
}

float xTRT::hitRadius(const xTRT::MSOS* msos,
                      const xTRT::DriftCircle* driftCircle) {
  float track_radius_biased = msos->localX();
  float hit_radius          = xTRT::Algorithm::get(xTRT::Acc::localX,driftCircle);
  if ( track_radius_biased < 0) return (-hit_radius);
  return hit_radius;
}

float xTRT::hitZR(const xAOD::TrackParticle* track, const xTRT::DriftCircle* driftCircle) {
  float ZR = -999;

  auto bec = std::abs(xTRT::Algorithm::get(xTRT::Acc::bec,driftCircle));
  // barrel
  if ( bec == 1 ) {
    float Xstraw = xTRT::Algorithm::get(xTRT::Acc::globalX,driftCircle);
    float Ystraw = xTRT::Algorithm::get(xTRT::Acc::globalY,driftCircle);
    float pos_r = std::sqrt(Xstraw*Xstraw + Ystraw*Ystraw);
    float pos_z = pos_r * std::tan(xTRT::PiHalf() - track->theta()) + track->z0();
    ZR = std::fabs(pos_z);
    if (ZR > 719.99) ZR = 719.99; // No zpos > 720mm
  }
  // endcaps
  else {
    double pos_z = xTRT::Algorithm::get(xTRT::Acc::globalZ,driftCircle);
    double pos_r = (pos_z - track->z0()) / std::tan(xTRT::PiHalf() - track->theta());
    ZR = fabs(pos_r);   // Pass to more global variable!
    if (ZR <  640.01) ZR =  640.01;  // No rpos <  640mm
    if (ZR > 1139.99) ZR = 1139.99;  // No rpos > 1140mm
  }

  return ZR;
}

float xTRT::hitL(const xAOD::TrackParticle* track,
                 const xTRT::MSOS* msos,
                 const xTRT::DriftCircle* driftCircle) {
  float trackTheta = track->theta();
  float trackPhi   = track->phi();
  float strawPhi   = xTRT::Algorithm::get(xTRT::Acc::localPhi,msos);
  float dcRad      = xTRT::Algorithm::get(xTRT::Acc::localX,driftCircle);
  float dcRadSq    = dcRad*dcRad;

  if ( dcRadSq > 4.0 ) {
    return 0; // outside of straw
  }

  auto bec = std::abs(xTRT::Algorithm::get(xTRT::Acc::bec,driftCircle));
  // barrel
  if ( bec == 1 ) {
    return 2*std::sqrt(4.0-dcRadSq)*1.0/std::fabs(std::sin(trackTheta));
  }
  // endcap
  float trigTerm = std::pow(std::sin(trackTheta),2)*std::pow(std::cos(trackPhi-strawPhi),2);
  if ( trigTerm > 1.0 ) trigTerm = 0.99999; // fix nans
  return 2*std::sqrt(4.0-dcRadSq)*1.0/std::sqrt(1.0-trigTerm);
}
