/** @file  Algorithm.h
 *  @brief xTRT::Algorithm class header
 *  @namespace xTRT
 *  @brief main TRTFramework namespace
 *  @class xTRT::Algorithm
 *  @brief Base class for running a
 *         TRTFramework analysis algorithm
 *
 *  This class has two purposes: one is to be a skeleton for the steps
 *  needed to run a TRTFramework algorithm. The second is the supply many
 *  helper functions and utilities for creating an algorithm for TRT
 *  work.
 *
 *  @author Douglas Davis < ddavis@cern.ch >
 */

#ifndef TRTFramework_Algorithm_h
#define TRTFramework_Algorithm_h

// C++
#include <memory>
#include <vector>
#include <map>
#include <functional>

// ATLAS
#include <TRTFramework/AtlasIncludes.h>

// TRTFramework
#include <TRTFramework/Utils.h>
#include <TRTFramework/Accessors.h>
#include <TRTFramework/HitSummary.h>
#include <TRTFramework/Config.h>
#include <TRTFramework/Helpers.h>

// ROOT
#include <TTree.h>

namespace xTRT {

  class Algorithm : public EL::Algorithm {

  private:
    xTRT::Config m_config;

    std::map<std::string,TObject*> m_objStore; //!

    int m_eventCounter;                 //!
    const xAOD::EventInfo* m_eventInfo; //!
    xAOD::TEvent* m_event;              //!
    xAOD::TStore* m_store;              //!

  private:
    asg::AnaToolHandle<IGoodRunsListSelectionTool>
    m_GRLToolHandle{"GoodRunsListSelectionTool/GRLTool",this}; //!
    asg::AnaToolHandle<CP::IPileupReweightingTool>
    m_PRWToolHandle{"CP::PileupReweightingTool/PRWTool",this}; //!
    asg::AnaToolHandle<TrigConf::ITrigConfigTool>
    m_trigConfToolHandle{"TrigConf::xAODConfigTool/xAODConfigTool",this}; //!
    asg::AnaToolHandle<Trig::TrigDecisionTool>
    m_trigDecToolHandle{"Trig::TrigDecisionTool/TrigDecisionTool",this}; //!
    asg::AnaToolHandle<Trig::IMatchingTool>
    m_trigMatchingToolHandle{"Trig::MatchingTool/TrigMatchingTool",this}; //!

    asg::AnaToolHandle<InDet::IInDetTrackSelectionTool>
    m_idtsTightPrimary{"InDet::InDetTrackSelectionTool/idtsTightPrimary",this}; //!
    asg::AnaToolHandle<InDet::IInDetTrackSelectionTool>
    m_idtsLoosePrimary{"InDet::InDetTrackSelectionTool/idtsLoosePrimary",this}; //!
    asg::AnaToolHandle<InDet::IInDetTrackSelectionTool>
    m_idtsLooseElectron{"InDet::InDetTrackSelectionTool/idtsLooseElectron",this}; //!
    asg::AnaToolHandle<InDet::IInDetTrackSelectionTool>
    m_idtsLooseMuon{"InDet::InDetTrackSelectionTool/idtsLooseMuon",this}; //!

  protected:
    std::string m_outputName{"TRTFrameworkOutput"};

  public:
    Algorithm();
    virtual ~Algorithm();

    /// delete copy constructor
    Algorithm(const Algorithm&) = delete;
    /// delete assignment operator
    Algorithm& operator=(const Algorithm&) = delete;
    /// delete move
    Algorithm(Algorithm&&) = delete;
    /// delete move assignment
    Algorithm& operator=(Algorithm&&) = delete;

    /// sets up the xTRT::Config class given a config file
    void feedConfig(const std::string fileName, bool print_conf = false, bool mcMode = false);

    /// Sets the treeOutput name for the EL::NTupleSvc
    void setTreeOutputName(const std::string name);

  protected:
    /** @addtogroup HistogramOutput Histogram Output
     *  @brief functions to easily create and modify histogram output
     *  @{
     */

    /// Creates a ROOT object to be stored.
    /**
     *  This allows easy creation by just calling the constructor of a
     *  ROOT object (such as a TH1F or TProfile). The name given to the
     *  object constructor will be used to manipulate the pointer to
     *  the copy which is stored.
     *
     *  Example:
     *  @code{.cpp}
     *  create(TH1F("h_avgMu","h_avgMu",100,0,100));
     *  @endcode
     *
     *  @param obj the ROOT output object (THx, TProfile, etc.)
     */
    template <typename T>
    void create(const T obj);

    /// Get access to a ROOT object which will be stored
    /**
     *  Using the name of the TObject staged for storage with the
     *  Algorithm::create function, retrieve and manipulate the object, used
     *  for e.g. filling a histogram.
     *
     *  Example:
     *  @code{.cpp}
     *  grab<TH1F>("h_avgMu")->Fill(averageMu(),weight());
     *  @endcode
     *
     *  @param name the name of the created ROOT object to update/modify.
     */
    template <typename T>
    T* grab(const std::string& name);

    /** @} */

    /// const pointer access to the configuration class
    const xTRT::Config* config() const;

  protected:
    /// creates and sets up the InDetTrackSelectionTool
    EL::StatusCode setupTrackSelectionTools();
    /// creates and sets up the GoodRunsListSelectionTool
    EL::StatusCode enableGRLTool();
    /// creates and sets up the PileupReweightingTool
    EL::StatusCode enablePRWTool();
    /// creates and sets up the ConfigTool, DecisionTool, and MatchingTool
    EL::StatusCode enableTriggerTools();

  public:
    /// checks if a track passes cuts defined in the config
    static bool passTrackSelection(const xAOD::TrackParticle* track, const xTRT::Config* conf);
    /// checks if an electron passes cuts defined in the config
    static bool passElectronSelection(const xAOD::Electron* electron, const xTRT::Config* conf);
    /// checks if a muon passes cuts defined in the config
    static bool passMuonSelection(const xAOD::Muon* muon, const xTRT::Config* conf);

  protected:

    /** @addtogroup ContainerGetters Container Getters
     *  @brief functions to easily grab (and define) different xAOD containers.
     *  @{
     */

    /// returns the raw track container (no selection applied)
    const xAOD::TrackParticleContainer* trackContainer();
    /// returns the raw electron container (no selection applied)
    const xAOD::ElectronContainer*      electronContainer();
    /// returns the raw electron container (no selection applied)
    const xAOD::MuonContainer*          muonContainer();

    /// use raw container to form deep copy containing only selected objects
    /**
     *  The class C must be a container of type T. This will create
     *  and return a deep copy of the raw container with name contName
     *  after applying the selection defined in the selector function.
     *
     *  @param raw the raw container
     *  @param selector the (static) function which applies the selection
     *  @param contName the name of the new deep copy container
     */
    template <class C, class T>
    const C* selectedContainer(const C* raw,
                               std::function<bool(const T*,const xTRT::Config*)> selector,
                               const std::string& contName);

    /// applies selectedContainer on tracks using config file settings
    const xAOD::TrackParticleContainer* selectedTracks();
    /// applies selectedContainer on electrons using config file settings
    const xAOD::ElectronContainer*      selectedElectrons();
    /// applies selectedContainer on muons using config file settings
    const xAOD::MuonContainer*          selectedMuons();

    /// get a new container of TrackParticles, Electrons, or Muons passing some IDTS cuts
    /**
     *  This will create and return a deep copy of selected objects
     *   living in a raw container after applying the set of cuts fed
     *   to this function. The cuts can be any combination of the four
     *   InDetTrackSelectionTool levels, defined in the enum
     *   xTRT::IDTSCut.
     *
     *  example:
     *
     *  @code{.cpp}
     *  auto tightPrimLooseMuons = selectedFromIDTScuts<xAOD::Muon>
     *     (muonContainer(),{xTRT::IDTSCut::LooseMuon,xTRT::IDTSCut::TightPrimary},"NewTightLooseMuons");
     *  @endcode
     *
     *  @param raw the raw container
     *  @param cuts the list (in braced-init-list form)
     *  @param contName the name of the new deep copy container
     */
    template <class T>
    const DataVector<T>* selectedFromIDTScuts(const DataVector<T>* raw,
                                              const std::initializer_list<xTRT::IDTSCut> cuts,
                                              const std::string& contName);

    /** @} */

  protected:
    /// check if the sample is MC
    bool isMC() const;
    /// check if the sample is Data (convenience function, opposite of isMC())
    bool isData() const;
    /// return the total event weight
    float eventWeight();
    /// return the average number of collisions per bunch crossing
    float averageMu();
    /// return the number of primary vertices
    std::size_t NPV() const;
    /// return whether the lumi block is good
    bool passGRL() const;

    /// get pointer to current xAOD::TEvent
    xAOD::TEvent* event();
    /// get pointer to current xAOD::TStore
    xAOD::TStore* store();
    /// get const pointer to the current event's xAOD::EventInfo
    const xAOD::EventInfo* eventInfo() const;

  protected:
    /// check for a nullptr and print a debug message
    /**
     * A lot of xAOD pointers have the potential to be null, this is
     * just a utliity function to test for a nullptr and use the ASG
     * message service to print a debug message
     *
     * @param ptr the pointer to check
     * @param message the message to print with ANA_MSG_DEBUG
     * @return true if good, false if nullptr.
     */
    template <class T>
    bool debug_nullptr(const T* ptr, const std::string& message = "") const;

    /// check for a nullptr and print a warning message
    /**
     * see xTRT::Algorithm::debugnullptr, but this prints a warning
     * message instead of a debug message.
     *
     * @param ptr the pointer to check
     * @param message the message to print with ANA_MSG_WARNING
     * @return true if good, false if nullptr
     */
    template <class T>
    bool warn_nullptr(const T* ptr, const std::string& message = "") const;

  protected:
    /// check whether a trigger fired
    bool triggerPassed(const std::string trigName) const;
    /// check whether a list of triggers fired
    bool triggersPassed(const std::vector<std::string>& trigNames) const;
    /// check if an electron matches to any of the single electron triggers in config file
    bool singleElectronTrigMatched(const xAOD::Electron* electron);
    /// check if a muon matches to any of the single muon triggers in config file
    bool singleMuonTrigMatched(const xAOD::Muon* muon);

  public:

  public:

    /** @addtogroup AuxHelp Aux Helpers
     *  @brief helpers for dealing with auxiliary data
     *  @{
     */

    /// grab aux data by using ConstAccessor and some object
    /**
     *  Using a ConstAccessor, look to see if the object has the
     *  auxdata and then return it. This is a wrapper around the
     *  ConstAccessor class to also print a warning if the auxdata
     *  isn't there.
     *
     *  Should be used with: @ref acc_Event, @ref acc_Track, @ref
     *  acc_DCandMSOS, @ref acc_MSOS, @ref acc_DriftCircle
     *
     *  Example usage (with the "bec" drift circle auxdata and
     *  "eProbability" track auxdata) given the existing xAOD objects:
     *
     *  @code{.cpp}
     *  auto track       = ...; // an xAOD::TrackParticle object
     *  auto driftCircle = ...; // an xTRT::DriftCircle object
     *  int brlec    = get(xTRT::Acc::bec,driftCircle);
     *  float trkOcc = get(xTRT::Acc::eProbabilityHT,track,"eProbabilityHT");
     *  @endcode
     *
     *  @param acc the const accessor object
     *  @param xobj the xAOD object to grab the auxdata from
     *  @param adn the auxdata variable name (only used for error message,
     *  default is blank)
     */
    template <class T>
    static const T get(const SG::AuxElement::ConstAccessor<T>& acc,
                       const SG::AuxElement* xobj, const std::string& adn = "");

    /// grab aux data from an xAOD object based on name
    /**
     *  This function will create a ConstAccessor to retrieve some aux
     *  data from the object. This is differen from the get function
     *  in that you must supply the return type, where with get the
     *  return type is deduced by the compiler from the already
     *  declared ConstAccessor.
     *
     *  @param xobj the xAOD object to retrieve the auxdata from
     *  @param adn the aux data variable name
     */
    template <typename T1, typename T2 = SG::AuxElement>
    static const T1 retrieve(const T2* xobj, const std::string& adn);

    /** @} */

  public:
    /// EventLoop API function
    virtual EL::StatusCode setupJob(EL::Job& job) override;
    /// EventLoop API function
    virtual EL::StatusCode fileExecute() override;
    /// EventLoop API function
    virtual EL::StatusCode histInitialize() override;
    /// EventLoop API function
    virtual EL::StatusCode changeInput(bool firstFile) override;
    /// EventLoop API function
    virtual EL::StatusCode initialize() override;
    /// EventLoop API function
    virtual EL::StatusCode execute() override;
    /// EventLoop API function
    virtual EL::StatusCode postExecute() override;
    /// EventLoop API function
    virtual EL::StatusCode finalize() override;
    /// EventLoop API function
    virtual EL::StatusCode histFinalize() override;

    ClassDefOverride(xTRT::Algorithm, 1);

  };

}

#include "Algorithm.icc"

#endif
