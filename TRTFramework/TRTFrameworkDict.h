// -*- c++ -*-

#include <TRTFramework/Algorithm.h>
#include <TRTFramework/TNPAlgorithm.h>
#include <TRTFramework/Config.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ class xTRT::Config+;
#pragma link C++ class xTRT::Algorithm+;
#pragma link C++ class xTRT::TNPAlgorithm+;
#endif
